# GlitchTip Error Factory


### Steps

1. Clone repo
2. Copy .env.example to .env and change DSN
3. Run `docker-compose pull`
4. Run `docker-compose up`
5. Test issue will be send during container startup
6. Visit `localhost:4000` to generate error and send another one


### Notes

- Running in production mode: change MIX_ENV=dev to MIX_ENV=prod
