defmodule GlitchtipErrorFactory.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  require Logger

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      GlitchtipErrorFactoryWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: GlitchtipErrorFactory.PubSub},
      # Start the Endpoint (http/https)
      GlitchtipErrorFactoryWeb.Endpoint
      # Start a worker by calling: GlitchtipErrorFactory.Worker.start_link(arg)
      # {GlitchtipErrorFactory.Worker, arg}
    ]

    Logger.debug("USING DSN: #{System.get_env("GLITCHTIP_ERROR_FACTORY_DSN")}")

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GlitchtipErrorFactory.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    GlitchtipErrorFactoryWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
