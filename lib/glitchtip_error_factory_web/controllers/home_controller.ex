defmodule GlitchtipErrorFactoryWeb.HomeController do
  use GlitchtipErrorFactoryWeb, :controller

  def index(conn, _params) do
    FakeModule.fake_function()
  end
end
