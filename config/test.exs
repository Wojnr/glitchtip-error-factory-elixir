import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :glitchtip_error_factory, GlitchtipErrorFactoryWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "w7ueCqPEy19kinX70/dvWOOw9eukzfD0RJsLMF1E1eeu5o1/2wmZ8Em7zScHhEJJ",
  server: false

# In test we don't send emails.
config :glitchtip_error_factory, GlitchtipErrorFactory.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
